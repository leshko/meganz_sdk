#!/bin/sh

CURRENTPATH=`pwd`

# Last commit:
# Add separate ASM file for XGETBV64 and CPUID64 to Visual Studio cryptdll project (GH #1240)
# Also see the comment in Commit 043208515799, where OgreTransporter made a comment about the deprecated cryptdll.vcxproj project.
# Commit:
# 1e20219ecddc64cf23e04f5cb990c49e1b0074c3 [1e20219e]
# Parents:
# af7d1050bf
# Author:
# Jeffrey Walton <noloader@gmail.com>
# Date:
# 22 October 2023 at 02:01:14 CEST

CRYPTOPP_VERSION="1e20219ecddc64cf23e04f5cb990c49e1b0074c3"

set -e

NPROCESSORS=$(getconf NPROCESSORS_ONLN 2>/dev/null || getconf _NPROCESSORS_ONLN 2>/dev/null)

if [ ! -e "${CRYPTOPP_VERSION}.tar.gz" ]
then
curl -LO "https://github.com/weidai11/cryptopp/archive/${CRYPTOPP_VERSION}.tar.gz"
fi

ARCHS="x86_64 arm64"

tar zxf ${CRYPTOPP_VERSION}.tar.gz

for ARCH in ${ARCHS}
do
pushd cryptopp-${CRYPTOPP_VERSION}
if [ $ARCH = "x86_64" ]; then
source TestScripts/setenv-ios.sh iPhoneSimulator ${ARCH}
elif [ $ARCH = "arm64" ]; then
sed -i '' $'75s/DEF_CPPFLAGS=\"-DNDEBUG\"/DEF_CPPFLAGS=\"-DNDEBUG -DCRYPTOPP_DISABLE_ARM_CRC32\"/' TestScripts/setenv-ios.sh
source TestScripts/setenv-ios.sh iPhone ${ARCH}
elif [ $ARCH = "arm64-simulator" ]; then
source TestScripts/setenv-ios.sh iPhoneSimulator ${ARCH}
fi;
mkdir -p "${CURRENTPATH}/bin/cryptopp/${ARCH}.sdk"
make -f GNUmakefile-cross lean -j${NPROCESSORS}
mv libcryptopp.a "${CURRENTPATH}/bin/cryptopp/${ARCH}.sdk"
mkdir -p "${CURRENTPATH}/bin/cryptopp/${ARCH}.sdk/include/cryptopp"
cp -f *.h "${CURRENTPATH}/bin/cryptopp/${ARCH}.sdk/include/cryptopp"
make clean
popd
done


mkdir -p lib

lipo -create "${CURRENTPATH}/bin/cryptopp/x86_64.sdk/libcryptopp.a" "${CURRENTPATH}/bin/cryptopp/arm64.sdk/libcryptopp.a" -output "${CURRENTPATH}/libcryptopp.a"

tar zxf ${CRYPTOPP_VERSION}.tar.gz
mkdir -p include/cryptopp || true
cp -f cryptopp-${CRYPTOPP_VERSION}/*.h include/cryptopp
rm -rf cryptopp-${CRYPTOPP_VERSION}
mv -f libcryptopp.a lib/

rm -rf bin
rm -rf ${CRYPTOPP_VERSION}.tar.gz

echo "Done."
