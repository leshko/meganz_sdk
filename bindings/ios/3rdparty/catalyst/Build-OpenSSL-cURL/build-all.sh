#!/bin/sh
		
set -e

sh build.sh -o 1.1.1i -c 7.74.0 -d -m

#  ./build.sh [-o <OpenSSL version>] [-c <curl version>] [-n <nghttp2 version>] [-d] [-e] [-x] [-h] [...]
#
#         -o <version>   Build OpenSSL version (default 1.1.1i)
#         -c <version>   Build curl version (default 7.74.0)
#         -n <version>   Build nghttp2 version (default 1.42.0)
#         -d             Compile without HTTP2 support
#         -e             Compile with OpenSSL engine support
#         -b             Compile without bitcode
#         -m             Compile Mac Catalyst library
#         -u <version>   Mac Catalyst iOS min target version (default 13.0)
#         -3             Compile with SSLv3
#         -s <version>   iOS min target version (default 8.0)
#         -t <version>   tvOS min target version (default 9.0)
#         -i <version>   macOS 86_64 min target version (default 11.1)
#         -a <version>   macOS arm64 min target version (default 11.1)
#         -x             No color output
#         -h             Show usage

echo "Done."

