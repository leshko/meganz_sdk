#!/bin/sh

UV_VERSION="1.23.0"

#inspired by:
# https://github.com/jasonacox/Build-OpenSSL-cURL

##############################################
SDK_VERSION=`xcrun -sdk macosx --show-sdk-version`
MACOS_X86_64_VERSION="10.15"
CATALYST_IOS_VERSION="13.0"
MACOS_ARM64_VERSION="11.0"

CURRENTPATH=`pwd`
ARCHS="x86_64 arm64"
PLATFORM="MacOSX"
DEVELOPER=`xcode-select -print-path`
##############################################

if [ ! -d "$DEVELOPER" ]; then
  echo "xcode path is not set correctly $DEVELOPER does not exist (most likely because of xcode > 4.3)"
  echo "run"
  echo "sudo xcode-select -switch <xcode path>"
  echo "for default installation:"
  echo "sudo xcode-select -switch /Applications/Xcode.app/Contents/Developer"
  exit 1
fi

case $DEVELOPER in
     *\ * )
           echo "Your Xcode path contains whitespaces, which is not supported."
           exit 1
          ;;
esac

case $CURRENTPATH in
     *\ * )
           echo "Your path contains whitespaces, which is not supported by 'make install'."
           exit 1
          ;;
esac

set -e

if [ ! -e "libuv-v${UV_VERSION}.tar.gz" ]
then
wget "http://dist.libuv.org/dist/v${UV_VERSION}/libuv-v${UV_VERSION}.tar.gz"
fi

for ARCH in ${ARCHS}
do

rm -rf libuv-v${UV_VERSION}
tar zxf libuv-v${UV_VERSION}.tar.gz
pushd "libuv-v${UV_VERSION}"

# Configure Build System
##############################################
export BUILD_TOOLS="${DEVELOPER}"
export BUILD_DEVROOT="${DEVELOPER}/Platforms/${PLATFORM}.platform/Developer"
export BUILD_SDKROOT="${BUILD_DEVROOT}/SDKs/${PLATFORM}${SDK_VERSION}.sdk"

export CC="${BUILD_TOOLS}/usr/bin/gcc -arch ${ARCH} -target ${ARCH}-apple-ios${CATALYST_IOS_VERSION}-macabi"
mkdir -p "${CURRENTPATH}/bin/${PLATFORM}${SDK_VERSION}-${ARCH}.sdk"
##############################################

# Build
##############################################
if [[ "${ARCH}" == "x86_64" ]]; then
export LDFLAGS="-Os -arch ${ARCH} -Wl,-dead_strip -target ${ARCH}-apple-ios${CATALYST_IOS_VERSION}-macabi -mmacosx-version-min=${MACOS_X86_64_VERSION} -L${BUILD_SDKROOT}/usr/lib"
export CFLAGS="-Os -arch ${ARCH} -pipe -no-cpp-precomp -isysroot ${BUILD_SDKROOT} -target ${ARCH}-apple-ios${CATALYST_IOS_VERSION}-macabi -mmacosx-version-min=${MACOS_X86_64_VERSION}"
fi

if [[ "${ARCH}" == "arm64" ]]; then
export LDFLAGS="-Os -arch ${ARCH} -Wl,-dead_strip -target ${ARCH}-apple-ios${CATALYST_IOS_VERSION}-macabi -mmacosx-version-min=${MACOS_ARM64_VERSION} -L${BUILD_SDKROOT}/usr/lib"
export CFLAGS="-Os -arch ${ARCH} -pipe -no-cpp-precomp -isysroot ${BUILD_SDKROOT} -target ${ARCH}-apple-ios${CATALYST_IOS_VERSION}-macabi -mmacosx-version-min=${MACOS_ARM64_VERSION}"
fi
##############################################

export CPPFLAGS="${CFLAGS} -I${BUILD_SDKROOT}/usr/include -DNDEBUG"
export CXXFLAGS="${CPPFLAGS}"

sh autogen.sh

if [ "${ARCH}" == "arm64" ]; then
./configure --host=aarch64-apple-darwin --enable-static --disable-shared
else
./configure --host=${ARCH}-apple-darwin --enable-static --disable-shared
fi

make -j8
cp -f .libs/libuv.a ${CURRENTPATH}/bin/${PLATFORM}${SDK_VERSION}-${ARCH}.sdk/
#make clean

popd

done


mkdir lib || true
lipo -create ${CURRENTPATH}/bin/MacOSX${SDK_VERSION}-x86_64.sdk/libuv.a ${CURRENTPATH}/bin/MacOSX${SDK_VERSION}-arm64.sdk/libuv.a -output ${CURRENTPATH}/lib/libuv.a

mkdir -p include || true
cp -f -R libuv-v${UV_VERSION}/include/ include/

rm -rf bin
rm -rf libuv-v${UV_VERSION}
rm -rf libuv-v${UV_VERSION}.tar.gz


echo "Done."
