#!/bin/sh

CURL_VERSION="7.74.0"

##############################################
SDK_VERSION=`xcrun -sdk macosx --show-sdk-version`
MACOS_X86_64_VERSION="10.15"
CATALYST_IOS_VERSION="13.0"
MACOS_ARM64_VERSION="11.0"

CURRENTPATH=`pwd`
ARCHS="arm64 x86_64"
PLATFORM="MacOSX"
DEVELOPER=`xcode-select -print-path`
##############################################

OPENSSL_PREFIX="${CURRENTPATH}"

if [ ! -d "$DEVELOPER" ]; then
  echo "xcode path is not set correctly $DEVELOPER does not exist (most likely because of xcode > 4.3)"
  echo "run"
  echo "sudo xcode-select -switch <xcode path>"
  echo "for default installation:"
  echo "sudo xcode-select -switch /Applications/Xcode.app/Contents/Developer"
  exit 1
fi

case $DEVELOPER in
     *\ * )
           echo "Your Xcode path contains whitespaces, which is not supported."
           exit 1
          ;;
esac

case $CURRENTPATH in
     *\ * )
           echo "Your path contains whitespaces, which is not supported by 'make install'."
           exit 1
          ;;
esac

set -e

if [ ! -e "curl-${CURL_VERSION}.tar.gz" ]
then
curl -LO "https://curl.haxx.se/download/curl-${CURL_VERSION}.tar.gz"
fi

for ARCH in ${ARCHS}
do

rm -rf curl-${CURL_VERSION}
tar zxf curl-${CURL_VERSION}.tar.gz
pushd "curl-${CURL_VERSION}"

# Do not resolve IPs!!
sed -i '' $'s/\#define USE_RESOLVE_ON_IPS 1//' lib/curl_setup.h

# Configure Build System
##############################################
export BUILD_TOOLS="${DEVELOPER}"
export BUILD_DEVROOT="${DEVELOPER}/Platforms/${PLATFORM}.platform/Developer"
export BUILD_SDKROOT="${BUILD_DEVROOT}/SDKs/${PLATFORM}${SDK_VERSION}.sdk"

export CC="${BUILD_TOOLS}/usr/bin/gcc -arch ${ARCH} -target ${ARCH}-apple-ios${CATALYST_IOS_VERSION}-macabi"
mkdir -p "${CURRENTPATH}/bin/${PLATFORM}${SDK_VERSION}-${ARCH}.sdk"
##############################################

# Build
##############################################
if [[ "${ARCH}" == "x86_64" ]]; then
export LDFLAGS="-Os -arch ${ARCH} -Wl,-dead_strip -target ${ARCH}-apple-ios${CATALYST_IOS_VERSION}-macabi -mmacosx-version-min=${MACOS_X86_64_VERSION} -L${BUILD_SDKROOT}/usr/lib"
export CFLAGS="-Os -arch ${ARCH} -pipe -no-cpp-precomp -isysroot ${BUILD_SDKROOT} -target ${ARCH}-apple-ios${CATALYST_IOS_VERSION}-macabi -mmacosx-version-min=${MACOS_X86_64_VERSION}"
fi

if [[ "${ARCH}" == "arm64" ]]; then
export LDFLAGS="-Os -arch ${ARCH} -Wl,-dead_strip -target ${ARCH}-apple-ios${CATALYST_IOS_VERSION}-macabi -mmacosx-version-min=${MACOS_ARM64_VERSION} -L${BUILD_SDKROOT}/usr/lib"
export CFLAGS="-Os -arch ${ARCH} -pipe -no-cpp-precomp -isysroot ${BUILD_SDKROOT} -target ${ARCH}-apple-ios${CATALYST_IOS_VERSION}-macabi -mmacosx-version-min=${MACOS_ARM64_VERSION}"
fi
##############################################

export CPPFLAGS="${CFLAGS} -I${BUILD_SDKROOT}/usr/include"
export CXXFLAGS="${CPPFLAGS}"

if [ "${ARCH}" == "arm64" ]; then
./configure --host="arm-apple-darwin" --enable-static --disable-shared --with-ssl=${OPENSSL_PREFIX} --with-zlib --disable-manual --disable-ftp --disable-file --disable-ldap --disable-ldaps --disable-rtsp --disable-proxy --disable-dict --disable-telnet --disable-tftp --disable-pop3 --disable-imap --disable-smtp --disable-gopher --disable-sspi --enable-ipv6 --disable-smb
else
./configure --host="${ARCH}-apple-darwin" --enable-static --disable-shared --with-ssl=${OPENSSL_PREFIX} --with-zlib --disable-manual --disable-ftp --disable-file --disable-ldap --disable-ldaps --disable-rtsp --disable-proxy --disable-dict --disable-telnet --disable-tftp --disable-pop3 --disable-imap --disable-smtp --disable-gopher --disable-sspi --enable-ipv6 --disable-smb
fi

make -j8
cp -f lib/.libs/libcurl.a ${CURRENTPATH}/bin/${PLATFORM}${SDK_VERSION}-${ARCH}.sdk/
#make clean

popd

done

mkdir lib || true
lipo -create ${CURRENTPATH}/bin/MacOSX${SDK_VERSION}-x86_64.sdk/libcurl.a ${CURRENTPATH}/bin/MacOSX${SDK_VERSION}-arm64.sdk/libcurl.a -output ${CURRENTPATH}/lib/libcurl.a

mkdir -p include/curl || true
cp -f curl-${CURL_VERSION}/include/curl/*.h include/curl/

rm -rf bin
rm -rf curl-${CURL_VERSION}


echo "Done."
