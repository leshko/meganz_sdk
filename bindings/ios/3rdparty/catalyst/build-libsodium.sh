#!/bin/sh

LIBSODIUM_VERSION="1.0.16"


#inspired by:
# https://github.com/jasonacox/Build-OpenSSL-cURL

##############################################
SDK_VERSION=`xcrun -sdk macosx --show-sdk-version`
MACOS_X86_64_VERSION="10.15"
CATALYST_IOS_VERSION="13.0"
MACOS_ARM64_VERSION="11.0"

CURRENTPATH=`pwd`
ARCHS="x86_64 arm64"
PLATFORM="MacOSX"
DEVELOPER=`xcode-select -print-path`
##############################################

if [ ! -d "$DEVELOPER" ]; then
  echo "xcode path is not set correctly $DEVELOPER does not exist (most likely because of xcode > 4.3)"
  echo "run"
  echo "sudo xcode-select -switch <xcode path>"
  echo "for default installation:"
  echo "sudo xcode-select -switch /Applications/Xcode.app/Contents/Developer"
  exit 1
fi

case $DEVELOPER in
     *\ * )
           echo "Your Xcode path contains whitespaces, which is not supported."
           exit 1
          ;;
esac

case $CURRENTPATH in
     *\ * )
           echo "Your path contains whitespaces, which is not supported by 'make install'."
           exit 1
          ;;
esac

set -e

if [ ! -e "libsodium-${LIBSODIUM_VERSION}.tar.gz" ]
then
wget "https://github.com/jedisct1/libsodium/releases/download/${LIBSODIUM_VERSION}/libsodium-${LIBSODIUM_VERSION}.tar.gz"
fi

for ARCH in ${ARCHS}
do

rm -rf libsodium-${LIBSODIUM_VERSION}
tar zxf libsodium-${LIBSODIUM_VERSION}.tar.gz
pushd "libsodium-${LIBSODIUM_VERSION}"

# Configure Build System
##############################################
export BUILD_TOOLS="${DEVELOPER}"
export BUILD_DEVROOT="${DEVELOPER}/Platforms/${PLATFORM}.platform/Developer"
export BUILD_SDKROOT="${BUILD_DEVROOT}/SDKs/${PLATFORM}${SDK_VERSION}.sdk"

export CC="${BUILD_TOOLS}/usr/bin/gcc -arch ${ARCH} -target ${ARCH}-apple-ios${CATALYST_IOS_VERSION}-macabi"
mkdir -p "${CURRENTPATH}/bin/${PLATFORM}${SDK_VERSION}-${ARCH}.sdk"
##############################################


# Build
##############################################
if [[ "${ARCH}" == "x86_64" ]]; then
export LDFLAGS="-Os -arch ${ARCH} -Wl,-dead_strip -target ${ARCH}-apple-ios${CATALYST_IOS_VERSION}-macabi -mmacosx-version-min=${MACOS_X86_64_VERSION} -L${BUILD_SDKROOT}/usr/lib"
export CFLAGS="-Os -arch ${ARCH} -pipe -no-cpp-precomp -isysroot ${BUILD_SDKROOT} -target ${ARCH}-apple-ios${CATALYST_IOS_VERSION}-macabi -mmacosx-version-min=${MACOS_X86_64_VERSION}"
fi

if [[ "${ARCH}" == "arm64" ]]; then
export LDFLAGS="-Os -arch ${ARCH} -Wl,-dead_strip -target ${ARCH}-apple-ios${CATALYST_IOS_VERSION}-macabi -mmacosx-version-min=${MACOS_ARM64_VERSION} -L${BUILD_SDKROOT}/usr/lib"
export CFLAGS="-Os -arch ${ARCH} -pipe -no-cpp-precomp -isysroot ${BUILD_SDKROOT} -target ${ARCH}-apple-ios${CATALYST_IOS_VERSION}-macabi -mmacosx-version-min=${MACOS_ARM64_VERSION}"
fi
##############################################


export CPPFLAGS="${CFLAGS} -I${BUILD_SDKROOT}/usr/include -DNDEBUG"
export CXXFLAGS="${CPPFLAGS}"

if [ "${ARCH}" == "arm64" ]; then
./configure --host=aarch64-apple-darwin --disable-shared --enable-minimal
else
./configure --host=${ARCH}-apple-darwin --disable-shared --enable-minimal
fi

make -j8

cp -f src/libsodium/.libs/libsodium.a ${CURRENTPATH}/bin/${PLATFORM}${SDK_VERSION}-${ARCH}.sdk/

popd

done


mkdir lib || true
lipo -create ${CURRENTPATH}/bin/MacOSX${SDK_VERSION}-x86_64.sdk/libsodium.a ${CURRENTPATH}/bin/MacOSX${SDK_VERSION}-arm64.sdk/libsodium.a -output ${CURRENTPATH}/lib/libsodium.a

cp -fR libsodium-${LIBSODIUM_VERSION}/src/libsodium/include/sodium* include

rm -rf bin
rm -rf libsodium-${LIBSODIUM_VERSION}

echo "Done."

